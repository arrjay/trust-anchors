#!/usr/bin/env bash

set -eux

# get a specific version of makeself as we supplant a file in it
MAKESELF_VER=2.4.0

# we attempt verification twice, once to see if we need to get the file, second to verify what we got.
gpg2 --verify "signatures/makeself-${MAKESELF_VER}.run.sig" "cache/makeself-${MAKESELF_VER}.run" || {
  curl -L -o "cache/makeself-${MAKESELF_VER}.run" \
   "https://github.com/megastep/makeself/releases/download/release-${MAKESELF_VER}/makeself-${MAKESELF_VER}.run"
}

gpg2 --verify "signatures/makeself-${MAKESELF_VER}.run.sig" "cache/makeself-${MAKESELF_VER}.run"

chmod +x "cache/makeself-${MAKESELF_VER}.run"

workdir=$(mktemp -d)
makeself_extract_dir=$(mktemp -d)

"./cache/makeself-${MAKESELF_VER}.run" --target "${makeself_extract_dir}" --keep --noexec

mkdir -p "${workdir}/support"

commitref=$(git rev-parse --verify HEAD)

git bundle create "${workdir}/trust-anchors.gitbundle" HEAD

# so, uh, don't use space top level, 'k?
# shellcheck disable=SC2046
git archive --format=tar HEAD $(find . -path ./.git -prune -o -path ./support -prune -o -path ./public -prune -o -path ./makeself -prune -o -path ./cache -prune -o \( -type d -wholename '*/*' \) -print) | tar xvf - -C "${workdir}"

SSH_OUTPUT_DIR="${workdir}/ssh_user" ./support/generate_ssh_userkeys.sh

cp support/install_anchors.sh "${workdir}/support"

rm -rf public

mkdir public

git log --show-signature > public/changelog.txt

cp public/changelog.txt "${workdir}"

find "${workdir}" -type l -exec rm {} \;

"${makeself_extract_dir}/makeself.sh" --sha256 --header ./support/ms/makeself-header.sh "${workdir}" public/install.run "RJ's Trust Anchors - ${commitref}" ./support/install_anchors.sh

rm -rf "${workdir}" || true
rm -rf "${makeself_extract_dir}" || true
