#!/usr/bin/env bash

set -e

tempdir=$(mktemp -d)

while IFS= read -r file ; do
  # skip .keygrip files
  case "${file}" in
    *.keygrip|*.pkcs8|*.sha1hash|*.md5hash) continue ;;
  esac

  # new sha256 fp, then sha1, then md5.
  ssh-keygen -l         -f "${file}"
  ssh-keygen -l -E sha1 -f "${file}"
  ssh-keygen -l -E md5  -f "${file}"

  IFS=' ' read -ra keydat < "${file}"
  case "${file}" in
    ssh_host/*)
      id="${keydat[0]}"
      alg="${keydat[1]}"
      data="${keydat[2]}"
    ;;
    ssh_user/*)
      alg="${keydat[0]}"
      data="${keydat[1]}"
      id="${keydat[*]:2}"
    ;;
  esac
  # convert into openssl / DER format for summation in AWS console format.
  echo "${alg} ${data}" > "${tempdir}/${id}.ssh"
  ssh-keygen -e -f "${tempdir}/${id}.ssh" -m pkcs8 | \
    openssl rsa -pubin -inform PEM -outform DER 2>/dev/null > "${tempdir}/${id}"
    ( cd "${tempdir}" && openssl md5 -c "${id}" && openssl sha1 -c "${id}" )
done <<< "$(find ssh_* -type f)"

rm -rf "${tempdir}"
