#!/usr/bin/env bash

set -e

type gpg  2>/dev/null 1>&2 && gpg=gpg
type gpg2 2>/dev/null 1>&2 && gpg=gpg2

# check that all the keys involved in pass keyrings are signed by key '0'
GNUPGHOME=$(mktemp -d)
export GNUPGHOME

keygrips=""

# we find keys 0 by looking directly at gpg/pass/zero*.ownertrust though.
ot_zero=( gpg/pass/zero*.asc.ownertrust )
# must have at least one
[ -e "${ot_zero[0]}" ]

for f in "${ot_zero[@]}" ; do
  read -r kg < "${f}"
  kg=${kg%%:*}
  kg=${kg:24}
  keygrips="${keygrips} ${kg}"
done

./support/install_anchors.sh pass-keys > /dev/null
./support/install_anchors.sh pass-ownertrust > /dev/null

for id in $($gpg --list-keys --with-colons | grep ^pub | cut -d: -f5) ; do
  gpgsigs=$($gpg --check-sigs "${id}" 2>/dev/null | grep "^sig!")
  for kg in ${keygrips} ; do
    echo "${gpgsigs}" | grep -q "${kg}" || {
      echo "failed to find signature from ${kg} in ${id}"
      exit 1
    }
  done
done

rm -rf "${GNUPGHOME}"
