#!/usr/bin/env bash

while IFS= read -r file ; do
  case "${file}" in
    support/ms/*) continue ;;
  esac
  shellcheck "${file}" || exit 1
done <<< "$(find support -name \*.sh)"
