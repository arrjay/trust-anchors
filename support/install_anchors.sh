#!/usr/bin/env bash

set -e

# when called, find script directory and run everything except ourselves.
# http://mywiki.wooledge.org/BashFAQ/028
# I know damn well it gives me only the first element but I'm not sure all versions of bash have this as an _array_
# shellcheck disable=SC2128
{
  if [[ -s "$BASH_SOURCE" ]] && [[ -x "$BASH_SOURCE" ]]; then
    # we found ourselves, do the needful.
    source_dir="$(dirname "$(readlink -f "$BASH_SOURCE")")/.."
  fi
}

# bail if unset
if [ -z "${source_dir}" ] ; then echo "failed to find self" 1>&2 ; exit 255 ; fi

type gpg  2>/dev/null 1>&2 && gpg=gpg
type gpg2 2>/dev/null 1>&2 && gpg=gpg2

check_gpg() {
  # shellcheck disable=SC2015
  [ -z "${gpg}" ] && { echo "cannot find gpg or gpg2" 1>&2 ; exit 1; } || true
}

inst_codesign() {
  check_gpg
  $gpg --import "${source_dir}/gpg"/codesign*.asc
}

inst_codesign_owner() {
  inst_codesign
  $gpg --import-ownertrust "${source_dir}/gpg"/codesign*.ownertrust
}

inst_pass() {
  check_gpg
  $gpg --import "${source_dir}/gpg/pass"/*.asc
}

inst_pass_owner() {
  check_gpg
  local trusts t
  trusts=( "${source_dir}/gpg/pass"/zero*.asc.ownertrust )
  for t in "${trusts[@]}" ; do
    $gpg --import-ownertrust "${t}"
  done
}

gpg_checksigs() {
  $gpg --check-sigs
}

show_changelog() {
  cat "${source_dir}/changelog.txt"
}

add_ssh_hosts() {
  local f sshlin host algo existing_ln
  [ -d "${HOME}/.ssh" ] || mkdir -p "${HOME}/.ssh"
  [ -f "${HOME}/.ssh/known_hosts" ] || touch "${HOME}/.ssh/known_hosts"
  for f in "${source_dir}/ssh_host"/* ; do
    read -r sshlin < "${f}"
    host="${sshlin%% *}"
    algo="${sshlin#${host} }"
    algo="${algo%% *}"
    existing_ln=$(grep "^${host}" "${HOME}/.ssh/known_hosts" || true)
    case "${existing_ln}" in
      *${algo}*) : ;;
      *)         echo "adding ${host}/${algo}" ; echo "${sshlin}" >> "${HOME}/.ssh/known_hosts" ;;
    esac
  done
}

show_userssh_sigs() {
  local fe base md5h
  for fe in "${source_dir}/ssh_user"/* ; do
    case "${fe}" in
      *.id) :        ;;
      *)    continue ;;
    esac
    base="${fe##*/}"
    base="${base%.id}"
    read -r md5h < "${source_dir}/ssh_user/${base}.md5hash"
    echo -n "${md5h##*)= } "
    echo "${base} "
  done
}

find_userssh_base() {
  local search expansion f b
  search="${1}"
  expansion=( "${source_dir}/ssh_user"/*"${search}"*.id )
  if [ -f "${expansion[1]}" ] ; then
    {
      echo "multiple expansions found..."
      for f in "${expansion[@]}" ; do
        b="${f##*/}" ; b="${b%.id}"
        echo "${b}"
      done
    } 1>&2
    return 1
  fi
  [ ! -f "${expansion[0]}" ] && { echo "no expansion found." 1>&2 ; return 1 ; }
  b="${expansion[0]##*/}" ; b="${b%.id}"
  echo "${b}"
}

get_sshu_thing() {
  local s path sfx
  s="${1}"
  sfx="${2}"
  path=$(find_userssh_base "${s}")
  [ "${path}" ] && cat "${source_dir}/ssh_user/${path}.${sfx}"
}

gpg_showsigs=""

case "${1}" in
  codesign)            inst_codesign ; gpg_showsigs="y" ;;
  codesign-ownertrust) inst_codesign_owner ; gpg_showsigs="y" ;;
  pass-keys)           inst_pass ; gpg_showsigs="y" ;;
  pass-ownertrust)     inst_pass_owner ; gpg_showsigs="y" ;;
  changelog)           show_changelog ;;
  well-known-hosts)    add_ssh_hosts ;;
  list-ssh-userkeys)   show_userssh_sigs ;;
  find-ssh-pub)        get_sshu_thing "${2}" id ;;
  find-gpgssh-keygrip) get_sshu_thing "${2}" keygrip ;;
  *)
cat << _EOF_ 1>&2
    please select an action to run
      codesign            - import codesign keys into gpg
      codesign-ownertrust - import codesign keys into gpg and auto-trust
      pass-keys           - import password-store keys into gpg
      pass-ownertrust     - import password-store keys into gpg, set trust on key "0"
      well-known-hosts    - add well known SSH hosts to ~/.ssh/known_hosts
      list-ssh-userkeys   - list all known ssh user keys
      find-ssh-pub        - display pubkey for user matching substring search
      find-gpgssh-keygrip - display gpg keygrip for user matching substring search
      changelog           - show changelog
_EOF_
  ;;
esac

# we can have an excess of success
# shellcheck disable=SC2015
[ -n "${gpg_showsigs}" ] && gpg_checksigs || true
