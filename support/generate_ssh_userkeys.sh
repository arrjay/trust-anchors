#!/usr/bin/env bash

set -e

# this script actually computes pubkeys and hashes of things like...gpg users ;)
# as such, we can run it when making the archive since not all downlevel systems have gpg2.1

# shut upppp ssh
umask 0077

tempdir=$(mktemp -d)

[ -n "${SSH_OUTPUT_DIR}" ] || SSH_OUTPUT_DIR=$(mktemp -d "${tempdir}/sshout.XXXXXX")

while IFS= read -r file ; do
  # any symlink here should be a link to a gpg key we are reusing the auth key from.
  [[ -L "${file}" ]] && {
    export GNUPGHOME="${tempdir}/${file}"
    mkdir -p "${GNUPGHOME}"
    # read key(s)
    gpg2 --import "${file}" 2> /dev/null
    # get user ids
    uids="$(gpg2 --list-keys --with-colons | cut -d: -f5,12)"
    for u in ${uids} ; do
      case "${u}" in
        *:a)
          h="${u::-2}"
          name="$(gpg2 --list-keys --with-colons "${h}" | awk -F: '$1 == "uid" { print $10; }')"
          IFS=' ' read -r -a sshfp <<< "$(gpg2 --export-ssh-key "${h}")"
          echo "${sshfp[0]}" "${sshfp[1]}" "${sshfp[2]}:${name}" > "${SSH_OUTPUT_DIR}/${name}.id"
          ssh-keygen -e -f "${SSH_OUTPUT_DIR}/${name}.id" -m pkcs8 | openssl rsa -pubin -inform PEM -outform DER > "${SSH_OUTPUT_DIR}/${name}.pkcs8"
          ( cd "${SSH_OUTPUT_DIR}" && openssl md5  -c "${name}.pkcs8" ) > "${SSH_OUTPUT_DIR}/${name}.md5hash"
          ( cd "${SSH_OUTPUT_DIR}" && openssl sha1 -c "${name}.pkcs8" ) > "${SSH_OUTPUT_DIR}/${name}.sha1hash"
          gpg2 --list-keys --with-colons --with-keygrip "${h}" | awk -F: '$5 == "'"${h}"'" { while (grp != "grp") {getline; grp = $1} print $10; }' \
            > "${SSH_OUTPUT_DIR}/${name}.keygrip"
          ;;
        *) : ;;
      esac
    done
  }
done <<< "$(find ssh_user)"

rm -rf "${tempdir}"
